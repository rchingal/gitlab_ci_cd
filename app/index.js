var express = require('express');
var app     = express();

app.get("/", function(req, res) {
  res.status(200).send("Hello From Gitlab CI CD Tutorial (NodeJS)");
});

app.listen(8081, function () {
	console.log("    Run server on http://localhost:8081")
});